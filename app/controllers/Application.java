package controllers;

import play.*;
import play.mvc.*;
import play.libs.*;
import play.libs.F.*;

import java.util.*;
import java.util.concurrent.*;

import models.*;

import views.html.*;
import models.MessagesCache;

public class Application extends Controller {
    private static MessagesCache messages = new MessagesCache();

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result poll(final Long lastMessageId) {
        Promise<Collection<Message>> promise = play.libs.Akka.future(
          new Callable<Collection<Message>>() {
            public Collection<Message> call() {
              Collection<Message> msgs = messages.getAllMessagesBeforeId(lastMessageId);
              try {
                while(msgs.size() == 0) {
                  msgs = messages.getAllMessagesBeforeId(lastMessageId);
                      Thread.sleep(2000);
                }
              } catch(Exception doCaralho){}

              return msgs;
            }
          }
        );
        return async(
          promise.map(
            new Function<Collection<Message>,Result>() {
              public Result apply(Collection<Message> i) {
                HashMap<String, Object> ret = new HashMap<String, Object>();

                ret.put("values", i);
                return ok(play.libs.Json.toJson(ret));
              }
            }
          )
        );
    }

    public static Result sendmsg(String message) {
      messages.addMessage(message);

      return ok();
    }

}
