package models;

import play.*;
import play.mvc.*;

import java.util.TreeMap;
import java.util.Collection;
import java.util.Date;

import models.Message;


public class MessagesCache {
    private TreeMap<Long, Message> messages;

    public MessagesCache() {
      this.messages = new TreeMap<Long, Message>();
    }

    public void addMessage(String messageText) {
      Message msg = new Message((new Date()).getTime(), messageText);

      System.out.println(messageText);

      this.messages.put(msg.getId(), msg);
    }

    public Collection<Message> getAllMessagesBeforeId(Long id) {
      return this.messages.tailMap(id + 1).values();
    }

    public void expireMessages() {
      this.messages = new TreeMap<Long, Message>(this.messages.tailMap((new Date().getTime())));
    }
}
