package models;

import play.*;
import play.mvc.*;

public class Message {
    private Long id;
    private String message;

    public Message(Long id, String message) {
      this.id = id;
      this.message = message;
    }

    public Long getId() {
      return this.id;
    }

    public String getMessage() {
      return this.message;
    }

    public void setId(Long val) {
      this.id = val;
    }

    public void setMessage(String val) {
      this.message = val;
    }

}
